Fuss-CRU
========

Fuss Client Release Upgrader — In-place upgrade of FUSS Clients
---------------------------------------------------------------

Quickstart
~~~~~~~~~~

Install the package fuss-cru on the fuss-server.

You can use the “Script” feature of OctoNet to trigger an update to a
client.

Installing fuss-cru from the package already adds a script clientupgrade
to octonet with the content of the file
``/var/www/fuss-data-conf/fcru/octonet_script``; add an execution and
program it as you would do normally.

Take into account that ``octonet-client`` will look for script to excute
at boot and every 5 minutes, so the execution is not immediate.

You can also test the procedure on a single machine just copying the
``octonet_script`` file on it, giving execute permission and
launching. Be aware that it will reboot the machine immediatly.

You can do this from any client connecting to it as root and executing the commands::

    FUSS_SERVER=$(ip r| grep 'default via'|awk '{print $3}')
    wget -O /root/octonet_script http://$FUSS_SERVER/fuss-data-conf/fcru/octonet_script
    chmod +x octonet_script
    /root/octonet_script


You can do this from the FUSS server, connecting to it as root and executing the commands::

    scp /var/www/fuss-data-conf/fcru/octonet_script root@client.name:
    ssh root@client.name /root/octonet_script


Packages caching
~~~~~~~~~~~~~~~~

Install and run fuss-server > 10.0.45, so that ``squid-deb-proxy`` is
installed and configured.

The octonet script already configures the client to use port 8000
instead of 8080 for the proxy on the clients, in ``/etc/apt/apt.conf``
(this is added by the fuss-client starting from release 11.0.3) with a
snippet like::

   Acquire::http::proxy "http://SER.VE.R.IP:8000";
   Acquire::https::proxy "http://SER.VE.R.IP:8000";
